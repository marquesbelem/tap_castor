﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementInPath : MonoBehaviour
{
    [SerializeField]
    private Transform[] points;

    [SerializeField]
    private float speed = 2f;

    public int pointsIdx = 0;

    public bool move;
    public bool goOrBack;

    void Start()
    {
        //transform.position = points[pointsIdx].transform.localPosition;
    }


    void Update()
    {
       if(move)
            Movement();
    }

    void Movement()
    {
        if (goOrBack)
        {
            if (pointsIdx <= points.Length - 1)
            {

                this.transform.position = Vector2.MoveTowards(this.transform.position,
                    points[pointsIdx].transform.position,
                    speed * Time.deltaTime);

                if (this.transform.position.x == points[pointsIdx].transform.position.x
                    && this.transform.position.y == points[pointsIdx].transform.position.y)
                {

                    pointsIdx += 1;
                }
            }  else
            {
                move = false;
            }
        }
        else
        {
            if (pointsIdx == -1)
                pointsIdx = points.Length;

            if (pointsIdx > 0)
            {
                if (this.transform.position.x == points[pointsIdx - 1].transform.position.x
                    && this.transform.position.y == points[pointsIdx - 1].transform.position.y)
                {
                    pointsIdx -= 1;
                }
                
                if (pointsIdx != 0)
                {
                    this.transform.position = Vector2.MoveTowards(this.transform.position,
                        points[pointsIdx - 1].transform.position,
                        speed * Time.deltaTime);
                }
            } else
            {
                move = false;
            }
        }
    }
}
