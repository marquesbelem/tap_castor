﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollTexture : MonoBehaviour
{
    [SerializeField]
    private float speed = 0.1f;

    private MeshRenderer meshRenderer;

    private void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
    }

    void Update()
    {
        float y = Time.time * speed;
        Vector2 offset = new Vector2(0, y);
        meshRenderer.sharedMaterial.SetTextureOffset("_MainTex", offset);
    }
}
