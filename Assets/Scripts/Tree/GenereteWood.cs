﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenereteWood : MonoBehaviour
{
    [SerializeField]
    private GameObject wood;

    string CastRay()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);

        if (hit) 
            return hit.collider.tag;

        return "";
        //if (hit)
            //Instantiate(wood, hit.transform.position, Quaternion.identity);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (CastRay() == "GenereteWood")
            {
                Instantiate(wood, this.transform.position, Quaternion.identity);
            }
        }
    }

}