﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToTree : MonoBehaviour
{
    private GameObject player;
    private GenereteWood genereteWood;
    public bool move;

    private void Start()
    {
        genereteWood = GetComponent<GenereteWood>();
    }

    string CastRay()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);
        if (hit) 
            return hit.collider.tag;

        return "";
    }

    void Update()
    {
        if (this.move)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (CastRay() == "Tree1")
                {
                    player = GameObject.FindGameObjectWithTag("Player");
                    MovePlayerForTree();
                }
                else if (CastRay() == "Tree2")
                {
                    player = GameObject.FindGameObjectWithTag("Player2");
                    MovePlayerForTree();
                }
            }
        }
    }


    void MovePlayerForTree()
    {
        player.GetComponent<MovementInPath>().goOrBack = false;
        player.GetComponent<MovementInPath>().pointsIdx = -1;
        player.GetComponent<MovementInPath>().move = true;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (player != null)
        {
            if (collision.gameObject.CompareTag(player.gameObject.tag))
            {
                genereteWood.enabled = true;
                this.move = false;
            }
        }
    }
}
