﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToDike : MonoBehaviour
{
    private GameObject player;

    private void Start()
    {
    }

    string CastRay()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);
        if (hit) 
            return hit.collider.tag;

        return "";
    }

    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            if (CastRay() == "Dike1")
            {
                player = GameObject.FindGameObjectWithTag("Player");
                MovePlayerForDike();
            }
            else if (CastRay() == "Dike2")
            {
                player = GameObject.FindGameObjectWithTag("Player2");
                MovePlayerForDike();
            }
        }
    }

    void MovePlayerForDike()
    {
        player.GetComponent<MovementInPath>().goOrBack = true;
        player.GetComponent<MovementInPath>().pointsIdx = 0;
        player.GetComponent<MovementInPath>().move = true;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
       
    }
}
